def measure(num = 1, &blk)
  start_time = Time.now
  num.times { blk.call }
  finish_time = Time.now

  (finish_time - start_time) / num
end
