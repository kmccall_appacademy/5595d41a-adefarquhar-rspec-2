def reverser(&blk)
  string = blk.call.split
  string.each(&:reverse!).join(' ')
end

def adder(num1 = 1, &blk)
  num2 = blk.call
  num1 + num2
end

def repeater(num = 1, &blk)
  num.times do |n|
    blk.call(n)
  end
end
